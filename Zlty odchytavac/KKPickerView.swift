//
//  PickerView.swift
//  Motto
//
//  Created by Kenan Karakecili on 4/15/16.
//  Copyright © 2016 Kenan Karakecili. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol KKPickerViewDelegate {
  func pickerDidSelectAction(_ row: Int)
}

class KKPickerView: UIPickerView {
  
  fileprivate var array: [String] = []
  var field: UITextField!
  var myDelegate: KKPickerViewDelegate?
  
  func setup(_ field: UITextField, array: [String]) {
    self.field = field
    self.array = array
    backgroundColor = UIColor.groupTableViewBackground
    showsSelectionIndicator = true
    delegate = self
    dataSource = self
    let toolBar = UIToolbar()
    toolBar.barStyle = UIBarStyle.default
    toolBar.tintColor = UIColor.blue
    toolBar.sizeToFit()
    let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
    toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
    field.inputView = self
    field.inputAccessoryView = toolBar
    field.tintColor = UIColor.clear
    if field.text?.characters.count > 0 {
      let row = self.array.index(of: field.text!)
      selectRow(row!, inComponent: 0, animated: false)
    } else {
      selectRow(0, inComponent: 0, animated: false)
    }
  }
  
  @objc fileprivate func doneButtonAction() {
    field.resignFirstResponder()
    let row = selectedRow(inComponent: 0)
    myDelegate!.pickerDidSelectAction(row)
  }
  
  @objc fileprivate func cancelButtonAction() {
    field.resignFirstResponder()
  }
}

extension KKPickerView: UIPickerViewDataSource, UIPickerViewDelegate {
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return array.count
  }
  
  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    let view2 = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
    let label = UILabel(frame: view2.frame)
    label.textAlignment = .center
    label.text = array[row]
    view2.addSubview(label)
    return view2
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return array[row]
  }
  
}
