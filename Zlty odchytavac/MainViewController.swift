//
//  MainViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 15.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import CoreData
import VersionTrackerSwift
import Alamofire
import SwiftOverlays
import PopupDialog

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {

    @IBOutlet weak var stationFromField: FormTextField!
    @IBOutlet weak var stationToField: FormTextField!
    @IBOutlet weak var dateTextField: FormTextField!
    @IBOutlet weak var tarifField: FormTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currencyControl: UISegmentedControl!
    
    var stations: [AnyObject] = []
    var lastRoutes: [LastRoute] = []
    var limitLastRoutes = 4
    var navLabel: UILabel!
    var selectedTextField: FormTextField!
    var notificationView: NotificationView!
    let tarifPickerView = KKPickerView()
    var blackOverlay : UIView?
    var tarifsValue = ["REGULAR", "CZECH_STUDENT_PASS_26", "CZECH_STUDENT_PASS_15", "ISIC", "CHILD", "SENIOR", "SENIOR_70", "YOUTH", "DISABLED", "DISABLED_ATTENDANCE", "EURO26"]
    var tarifsText = ["Dospelý", "Žiacky preukaz <26", "Žiacky preukaz <15", "ISIC", "Dieťa <15", "Senior >60", "Senior >70", "Mládežník <26", "ŤZP (ŤZP/S)", "Sprievodca ŤZP/S", "Euro 26/Alive"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let user = self.getUser()
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.showNotification(_:)), name:NSNotification.Name(rawValue: "notification"), object: nil)
        navigationItem.hidesBackButton = true
        
        tarifPickerView.myDelegate = self
        tarifField.delegate = self
        stationToField.inputView = UIView()
        stationFromField.inputView = UIView()
        tarifField.text = user?.last_tarif
        
        dateTextField.inputView = UIView()
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = dateFormatter.string(from: currentDate)
        
        if(self.view.frame.size.width < 375) {
            limitLastRoutes = 3
        }
        
        self.navigationController!.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
        
        if(Int((user?.last_build)!) != Int(VersionTracker.currentBuild())!) {
            let size = CGSize(width: 30, height:30)
            
            startAnimating(size, message: NSLocalizedString("download_stations", comment: "Loading..."), type: .ballClipRotatePulse)
            self.updateStationsFromApi()
            user?.last_build = Int16(VersionTracker.currentBuild())!
            do {
                try user?.managedObjectContext?.save()
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            self.loadAllStations()
        }
        
        blackOverlay = UIView(frame: view.frame)
        blackOverlay!.backgroundColor = UIColor.black
        blackOverlay!.alpha = 0.6
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBar = self.navigationController?.navigationBar
        navLabel = UILabel(frame: CGRect(x: 0, y: 5, width: navigationBar!.frame.width, height: navigationBar!.frame.height-5))
        navLabel.textAlignment = .center
        navLabel.text = NSLocalizedString("title_nav", comment: "- YELLOW CATCHER -")
        navLabel.font = UIFont(name: "BloggerSans-Bold", size: 18)
        navigationBar!.addSubview(navLabel)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navLabel.removeFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let heightCell = Int((self.view.frame.size.height/2-50))/limitLastRoutes+1
        self.tableView.rowHeight = CGFloat(heightCell)
        self.loadLastRoutes()
        let navBarHeight = UIApplication.shared.statusBarFrame.height+self.navigationController!.navigationBar.frame.height
        appDelegate.NAV_HEIGHT = Int(navBarHeight)
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: navBarHeight))
        
        if let notification = UserDefaults.standard.value(forKey: "notification") {
            let notif = notification as! [String:AnyObject]
            self.perform(#selector(self.showTicket), with: notif, afterDelay: 1.0)
            UserDefaults.standard.removeObject(forKey: "notification")
        }
    }
    
    func updateStationsFromApi() -> Void {
        self.deleteAllStations()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Alamofire.request(String(appDelegate.API_URL+"stations"), parameters: nil)
            .validate()
            .responseString {response in
                var json: AnyObject?
                if(response.result.isSuccess) {
                    json = response.result.value!.JSONString
                } else {
                    let filepath = Bundle.main.path(forResource: "stations", ofType: "json")
                    do {
                        let contents = try NSString(contentsOfFile: filepath!, usedEncoding: nil) as String
                        json = contents.JSONString
                    } catch {
                        print("Error loading stations.json")
                    }
                }
                let jsonArray = (json as! NSArray) as Array
                
                let managedContext = appDelegate.managedObjectContext
                
                for station in jsonArray {
                    let obj = station as! NSDictionary
                    let newStation = NSEntityDescription.insertNewObject(forEntityName: "Station", into: managedContext) as! Station
                    newStation.title = obj["title"]! as? String
                    newStation.id = (obj["id"] as? NSString)! as String
                    newStation.latitude = obj["latitude"]! as? String
                    newStation.longitude = obj["longitude"]! as? String
                    newStation.priority = Int16(obj["priority"]! as! Int)
                    newStation.countryCode = obj["countryCode"]! as? String
                    do {
                        try managedContext.save()
                    } catch {
                        fatalError("Failure to save context new station: \(error)")
                    }
                }
                self.loadAllStations()
                self.stopAnimating()
            }
    }
    
    func loadAllStations() -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<Station>
        if #available(iOS 10.0, *) {
            request = Station.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "Station")
        }
        request.sortDescriptors = [NSSortDescriptor(key: "priority", ascending: false)]
        
        do {
            stations = try managedContext.fetch(request)
        } catch let error {
            print("Error in loadAllStations: "+error.localizedDescription)
        }
    }
    
    func deleteAllStations() -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        }
        catch let error as NSError {
            print("Error in deleteAllStations: "+error.localizedDescription)
        }
    }
    
    @IBAction func changeStations(_ sender: AnyObject) {
        
        let tempStation = self.stationFromField.text
        self.stationFromField.text = self.stationToField.text
        self.stationToField.text = tempStation
        self.stationFromField.showFocus()
        self.stationToField.showFocus()
    }
    
    @IBAction func stationFromFieldAction(_ sender: AnyObject) {
        selectedTextField = sender as! FormTextField
        performSegue(withIdentifier: "showStations", sender: nil)
    }
    
    @IBAction func stationToFieldAction(_ sender: AnyObject) {
        selectedTextField = sender as! FormTextField
        performSegue(withIdentifier: "showStations", sender: nil)
    }
    
    @IBAction func dateFieldAction(_ sender: AnyObject) {
        selectedTextField = sender as! FormTextField
        performSegue(withIdentifier: "showDatePicker", sender: nil)
    }
    
    @IBAction func showRoutes(_ sender: AnyObject) {
        if(stationFromField.text?.characters.count == 0) {
            notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops...A problem has occured")
            notificationView.textLabelText = NSLocalizedString("error_missing_from", comment: "Please, fill in where you come from.")
            UIViewController.showNotificationOnTopOfStatusBar(notificationView, duration: 1.5)
        } else if(stationToField.text?.characters.count == 0) {
            notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops...A problem has occured")
            notificationView.textLabelText = NSLocalizedString("error_missing_to", comment: "Please, fill in your destination.")
            UIViewController.showNotificationOnTopOfStatusBar(notificationView, duration: 1.5)
        } else {
            performSegue(withIdentifier: "showRoutes", sender: nil)
        }
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStations" {
            let controller = segue.destination as! StationsViewController
            controller.stations = stations
            controller.textField = selectedTextField
        } else if segue.identifier == "showRoutes" {
            let controller = segue.destination as! RoutesViewController
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            controller.date = dateFormatter.date(from: dateTextField.text!)
            controller.tarif = tarifsValue[tarifsText.index(of: tarifField.text!)!]
            controller.tarif_text = tarifField.text
            controller.currency = currencyControl.titleForSegment(at: currencyControl.selectedSegmentIndex)
            for station in stations {
                let obj = station as! Station
                if(obj.title == stationFromField.text) {
                    controller.stationFrom = obj
                } else if(obj.title == stationToField.text) {
                    controller.stationTo = obj
                }
                
                if controller.stationFrom != nil && controller.stationTo != nil {
                    break
                }
                
            }
        } else if segue.identifier == "showDatePicker" {
            let controller = segue.destination as! DatePickerViewController
            controller.textField = selectedTextField
        }
    }
    
    //MARK - Favourite Table View
    @IBAction func useFavouriteAction(_ sender: AnyObject) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lastRoutes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LastViewCell
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 0.8471, green: 0.9098, blue: 0.8706, alpha: 1.0)
        cell.selectedBackgroundView = bgColorView
        let index = indexPath.row as Int
        let obj = lastRoutes[index] 
        cell.fromStationLabel.text = NSLocalizedString("from", comment: "From: ")+obj.stationFrom!
        cell.toStationLabel.text = NSLocalizedString("to", comment: "To: ")+obj.stationTo!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let index = (indexPath as NSIndexPath).row as Int
        let obj = lastRoutes[index] 
        self.stationFromField.text = obj.stationFrom
        self.stationToField.text = obj.stationTo
        self.stationFromField.showFocus()
        self.stationToField.showFocus()
    }

    func loadLastRoutes() -> Void {
        lastRoutes.removeAll()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<LastRoute>
        if #available(iOS 10.0, *) {
            request = LastRoute.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "LastRoute")
        }
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        do {
            var exist = false
            let routesUnFiltered = try managedContext.fetch(request)
            for lastRoute in routesUnFiltered {
                for lastRouteFilter in lastRoutes {
                    if(lastRoute.stationFrom == lastRouteFilter.stationFrom && lastRoute.stationTo == lastRouteFilter.stationTo) {
                        exist = true
                        break
                    }
                }
                if(!exist) {
                    lastRoutes.append(lastRoute)
                }
                if(lastRoutes.count == limitLastRoutes) {
                    break
                }
                exist = false
            }
        } catch let error {
            print("Error in loadLastRoutes: "+error.localizedDescription)
        }
        
        self.tableView.reloadData()
    }
    
    func showNotification(_ notification: Notification) -> Void {
        
        self.dismiss(animated: true, completion: {
            self.perform(#selector(self.showTicket), with: notification.userInfo, afterDelay: 0.5)
            return
        });
        
        self.perform(#selector(self.showTicket), with: notification.userInfo, afterDelay: 1.0)

    }
    
    //MARK - Ticket
    func showTicket(_ notification: NSDictionary) -> Void {
        let ticketVC = TicketViewController(nibName: "TicketViewController", bundle: nil)
        ticketVC.mainVC = self
        let popup = PopupDialog(viewController: ticketVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        present(popup, animated: true, completion: nil)
        ticketVC.loadTicket(String(format:"%@", (notification["route_id"]! as! NSNumber)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK - Tarif Picker View
extension MainViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tarifField {
            tarifPickerView.setup(textField, array: tarifsText)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tarifField {
            view.addSubview(blackOverlay!)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tarifField {
            blackOverlay?.removeFromSuperview()
        }
    }
}

extension MainViewController: KKPickerViewDelegate {
    
    func pickerDidSelectAction(_ row: Int) {
        if tarifPickerView.field == tarifField {
            tarifField.text = tarifsText[row]
        }
    }
    
}
