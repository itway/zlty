//
//  SaveRoutesViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 27.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftOverlays

class SaveRoutesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var routes: [AnyObject] = []
    var navLabel: UILabel!
    var loader: NVActivityIndicatorView!
    var notificationView: NotificationView!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("download_stations", comment: "Loading..."))
        tableView.addSubview(refreshControl!)
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2-50, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(red: 0.2275, green: 0.2, blue: 0.3294, alpha: 1.0)
        self.view.addSubview(loader)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT))
        
        loader.startAnimating()
        self.loadRoutes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBar = self.navigationController?.navigationBar
        navLabel = UILabel(frame: CGRect(x: 0, y: 5, width: navigationBar!.frame.width, height: navigationBar!.frame.height-5))
        navLabel.textAlignment = .center
        navLabel.text = NSLocalizedString("saved_connections", comment: "- SAVED CONNECTIONS -")
        navLabel.font = UIFont(name: "BloggerSans-Bold", size: 18)
        navigationBar!.addSubview(navLabel)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navLabel.removeFromSuperview()
    }
    
    func loadRoutes() -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let user = self.getUser()
        let urlString = appDelegate.API_URL+"userRoutes/"+(user?.token)!
        Alamofire.request(urlString, parameters: nil)
            .validate()
            .responseString {response in
                if(response.result.isSuccess) {
                    let json = response.result.value!.JSONString
                    self.routes = (json as! NSArray) as Array
                    self.tableView.reloadData()
                } else {
                    self.notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops... A problem has occured")
                    self.notificationView.textLabelText = NSLocalizedString("error_connection_text", comment: "We are working to correct the issue.")
                    UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 1.5)
                }
                self.loader.stopAnimating()
                self.refreshControl.endRefreshing()
        }
    }
    
    //MARK - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RoutesViewCell
        let index = (indexPath as NSIndexPath).row as Int
        let obj = routes[index] as! NSDictionary
        
        let stationFrom = self.getStationByID(id: obj["stationFrom"] as! String)
        let stationTo = self.getStationByID(id: obj["stationTo"] as! String)
        cell.selectionStyle = .none
        cell.arrivalLabel.text = obj["arrival"] as? String
        cell.departureLabel.text = obj["departure"] as? String
        cell.seatsLabel.text = obj["seats"] as? String
        cell.fromStationLabel.text = NSLocalizedString("from", comment: "From: ")+(stationFrom?.title!)!
        cell.toStationLabel.text = NSLocalizedString("to", comment: "To: ")+(stationTo?.title)!
        if((obj["currency"] as? String) == "EUR") {
            cell.priceLabel.text = (obj["price"] as? String)!+"€"
        } else {
            cell.priceLabel.text = (obj["price"] as? String)!+"Kč"
        }
        
        let typeString = obj["type"] as? String
        if(typeString == "Coach" || typeString == "Autobus") {
            cell.typeIcon.image = UIImage(named: "IconCoach")
        } else if(typeString == "Rail car" || typeString == "Vlak") {
            cell.typeIcon.image = UIImage(named: "IconTrain")
        } else {
            cell.typeIcon.image = UIImage(named: "IconBusTrain")
        }
        
        if((obj["seats"] as? String) == "1") {
            cell.freeTextLabel.text = NSLocalizedString("1_ticket", comment: " available ticket")
        } else if((obj["seats"] as? String) == "2" || (obj["seats"] as? String) == "3" || (obj["seats"] as? String) == "4") {
            cell.freeTextLabel.text = NSLocalizedString("2_ticket", comment: " available tickets")
        } else {
            cell.freeTextLabel.text = NSLocalizedString("x_tickets", comment: " available tickets")
        }
        
        cell.typeIcon.image = cell.typeIcon.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        if((obj["seats"] as? String) == "0") {
            let redColor = UIColor(red: 0.8157, green: 0.0078, blue: 0.1059, alpha: 1.0)
            cell.typeIcon.tintColor = redColor
            cell.arrivalLabel.textColor = redColor
            cell.departureLabel.textColor = redColor
            cell.delimiterView.backgroundColor = redColor
            cell.leftView.backgroundColor = UIColor(red: 0.9529, green: 0.8902, blue: 0.898, alpha: 1.0)
            cell.priceTextLabel.isHidden = true
            cell.priceLabel.text = ""
            cell.seatsLabel.text = ""
            cell.freeTextLabel.text = NSLocalizedString("sold_out", comment: "Sold out")
            cell.buyButton.isEnabled = false
            
        } else {
            let grayColor = UIColor(red: 0.4627, green: 0.4627, blue: 0.4627, alpha: 1.0)
            cell.typeIcon.tintColor = grayColor
            cell.arrivalLabel.textColor = grayColor
            cell.departureLabel.textColor = grayColor
            cell.delimiterView.backgroundColor = grayColor
            cell.leftView.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)
            cell.priceTextLabel.isHidden = false
            cell.buyButton.isEnabled = true
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: obj["date"] as! String)
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateFormatted = dateFormatter.string(from: date!)
        
        cell.dateLabel.text = dateFormatted
        
        cell.buyButton.tag = index
        
        self.view.needsUpdateConstraints()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            let index = (indexPath as NSIndexPath).row as Int
            let obj = routes[index] as! NSDictionary
            
            routes.remove(at: index)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            let parameters: Parameters = ["user_id": obj["user_id"] as! Int as AnyObject, "route_id" : obj["route_id"] as! Int as AnyObject]
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            Alamofire.request(String(appDelegate.API_URL+"deleteSavedRoute"), method: .post, parameters: parameters)
                .validate()
                .responseString {response in
                    if(!response.result.isSuccess){
                        self.notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops... A problem has occured")
                        self.notificationView.textLabelText = NSLocalizedString("error_connection_text", comment: "We are working to correct the issue.")
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 1.5)
                    }
            }            
        }
    }
    
    @IBAction func buyAction(_ sender: AnyObject) {
        
        let obj = routes[sender.tag] as! NSDictionary
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: obj["date"] as! String)
        
        dateFormatter.dateFormat = "yyyyMMdd"
        let dateFormatted = dateFormatter.string(from: date!)
        let stationFrom = obj["stationFrom"] as! String
        let stationTo = obj["stationTo"] as! String
        let tarif = obj["tarif"] as! String
        
        let urlString = "http://jizdenky.studentagency.sk/m/Booking/from/"+stationFrom+"/to/"+stationTo+"/tarif/"+tarif+"/departure/"+dateFormatted+"/retdep/"+dateFormatted+"/return/False/credit/True"
        
        UIApplication.shared.openURL(URL(string: urlString)!)
        
    }
    
    func refreshTable() {
        self.loadRoutes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
