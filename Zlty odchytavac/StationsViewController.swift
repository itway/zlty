//
//  StationsViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 20.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import Foundation
import UIKit

class StationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var stations: [AnyObject] = []
    var stationsAuto: [AnyObject] = []
    var textField: FormTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: FormTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        searchBar.delegate = self
        searchBar.clearButtonMode = .whileEditing
        searchBar.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor(red:0.30, green:0.28, blue:0.40, alpha:1.0)])
//        searchBar.setImage(UIImage(named: "ico-cancel"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)
        stationsAuto = stations

        searchBar.becomeFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let substring = (searchBar.text! as NSString).replacingCharacters(in: range, with: string)
        if(substring.characters.count > 0) {
            searchAutocompleteEntriesWithSubstring(substring.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current))
        } else {
            stationsAuto = stations
            tableView.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        stationsAuto = stations
        tableView.reloadData()
        return true
    }
    
    func searchAutocompleteEntriesWithSubstring(_ substring: String) {
        stationsAuto.removeAll(keepingCapacity: false)
        
        for station in stations {
            let obj = station as! Station
            if obj.title?.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).range(of: substring) != nil {
                stationsAuto.append(station)
            }
        }
        
        tableView.reloadData()
    }
    @IBAction func closeModal(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    
    //MARK - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stationsAuto.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StationsViewCell
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 0.8471, green: 0.9098, blue: 0.8706, alpha: 1.0)
        cell.selectedBackgroundView = bgColorView
        let index = (indexPath as NSIndexPath).row as Int
        let obj = stationsAuto[index] as! Station
        cell.titleLabel.text = obj.title
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let index = (indexPath as NSIndexPath).row as Int
        let obj = stationsAuto[index] as! Station
        textField.text = obj.title
        obj.priority = obj.priority+1
        do {
            try obj.managedObjectContext?.save()
        } catch {
            fatalError("Failure to save increment priority in station: \(error)")
        }
        self.dismiss(animated: true, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
