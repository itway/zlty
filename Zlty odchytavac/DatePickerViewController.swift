//
//  DatePickerViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 19.10.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DatePickerViewController: UIViewController{

    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    var myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
    var textField: FormTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en-US") as Locale!
        var selectedDate = dateFormatter.date(from: textField.text!)
        selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: selectedDate!)
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.registerCellViewXib(file: "CellView")
        calendarView.selectDates([selectedDate!])
        calendarView.cellInset = CGPoint(x: 0, y: 0)
        myCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        
        calendarView.visibleDates { (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    @IBAction func closeModal(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func doneAction(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateFormated = dateFormatter.string(from: calendarView.selectedDates[0])
        textField.text = dateFormated
        self.dismiss(animated: true, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DatePickerViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let startDate = Date()
        let calendar = Calendar.current
        let endDate = calendar.date(byAdding: .year, value: 1, to: startDate)
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate!,
                                                 numberOfRows: 6,
                                                 calendar: myCalendar,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: .monday)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        let myCustomCell = cell as! CellView
        myCustomCell.dayLabel.text = cellState.text
        if cellState.dateBelongsTo == .thisMonth {
            myCustomCell.dayLabel.textColor = UIColor.black
            let calendar = Calendar.current
            let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
            if(date.compare(yesterday!) == .orderedDescending) {
                myCustomCell.isUserInteractionEnabled = true
                myCustomCell.dayLabel.textColor = UIColor.black
                
            } else {
                myCustomCell.isUserInteractionEnabled = false
                myCustomCell.dayLabel.textColor = UIColor.gray
            }
        } else {
            myCustomCell.dayLabel.textColor = UIColor.white
            myCustomCell.isUserInteractionEnabled = false
        }
        
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
    }
    
    func handleCellSelection(view: JTAppleDayCellView?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else {
            return
        }
        if cellState.isSelected {
            myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.width/2
            myCustomCell.selectedView.isHidden = false
        } else {
            myCustomCell.selectedView.isHidden = true
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first else {
            return
        }
        let month = myCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        let year = myCalendar.component(.year, from: startDate)
        monthLabel.text = (monthName + " " + String(year)).uppercased()
    }
}
