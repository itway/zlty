//
//  SplashViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 13.09.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import SwiftyGif

class SplashViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var titleLabel: UILabel! = nil
    var subTitleLabel: UILabel! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        let gif = UIImage(gifName: "splash")
        self.imageView.setGifImage(gif, loopCount:2)
        self.perform(#selector(SplashViewController.redirect), with: nil, afterDelay: 2.5)
        
        titleLabel = UILabel(frame: CGRect(x: 450, y: 150, width: 300, height: 40))
        titleLabel.textAlignment = .left
        titleLabel.text = NSLocalizedString("splash_title", comment: "YELLOW CATCHER")
        titleLabel.font = UIFont(name: "BloggerSans-Bold", size: 28)
        titleLabel.textColor = UIColor(red:1.00, green:0.86, blue:0.06, alpha:1.0)
        titleLabel.textAlignment = .right
        self.imageView.addSubview(titleLabel)
        
        subTitleLabel = UILabel(frame: CGRect(x: 450, y: 180, width: 300, height: 40))
        subTitleLabel.textAlignment = .left
        subTitleLabel.text = NSLocalizedString("splash_subtitle", comment: "...the catcher of Regiojet tickets")
        subTitleLabel.font = UIFont(name: "BloggerSans", size: 16)
        subTitleLabel.textColor = UIColor(red:0.77, green:0.64, blue:0.07, alpha:1.0)
        subTitleLabel.textAlignment = .right
        
        self.imageView.addSubview(subTitleLabel)
        
        self.perform(#selector(SplashViewController.inSubtitle), with: nil, afterDelay: 0.5)
        UIView.animate(withDuration: 1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.titleLabel.frame = CGRect(x: 0, y: 150, width: 300, height: 40)
        }, completion: { (finished) -> Void in
            self.perform(#selector(SplashViewController.outTitle), with: nil, afterDelay: 2.0)
        })
    }
    
    func redirect() -> Void {
        self.performSegue(withIdentifier: "showApp", sender: self)
    }
    
    func outTitle() {
        UIView.animate(withDuration: 1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.titleLabel.frame = CGRect(x: -300, y: 150, width: 300, height: 40)
        }, completion: nil)
    }
    
    func inSubtitle() {
        UIView.animate(withDuration: 0.8,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.subTitleLabel.frame = CGRect(x: 0, y: 180, width: 300, height: 40)
        }, completion: { (finished) -> Void in
            self.perform(#selector(SplashViewController.outSubTitle), with: nil, afterDelay: 2.0)
        })
    }
    
    func outSubTitle() {
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.subTitleLabel.frame = CGRect(x: -300, y: 180, width: 300, height: 40)
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.imageView.stopAnimatingGif()
    }
    
}
