//
//  CreditsViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 4.12.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import MessageUI

class CreditsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    var navLabel: UILabel!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBar = self.navigationController?.navigationBar
        navLabel = UILabel(frame: CGRect(x: 0, y: 5, width: navigationBar!.frame.width, height: navigationBar!.frame.height-5))
        navLabel.textAlignment = .center
        navLabel.text = NSLocalizedString("credits", comment: "- ABOUT -")
        navLabel.font = UIFont(name: "BloggerSans-Bold", size: 18)
        navigationBar!.addSubview(navLabel)
    }
    
    override func viewWillLayoutSubviews() {
        
        let language = NSLocale.current.languageCode!
        if(language == "sk" || language == "cz") {
            heightContentConstraint.constant = 710
        } else {
            heightContentConstraint.constant = 800
        }
    }
 
    @IBAction func sendEmail(_ sender: Any) {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients(["support@odchytavac.com"])
        present(mailVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        navLabel.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
