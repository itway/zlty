//
//  TicketViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 01.08.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import Alamofire

class TicketViewController: UIViewController {

    @IBOutlet weak var delimiterY: DottedView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var freeSeatsLabel: UILabel!
    @IBOutlet weak var ticketInfoLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var freeSeatsTitle: UILabel!
    
    var loader: NVActivityIndicatorView!
    var mainVC: UIViewController!
    var ticket: NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-18, y: self.view.frame.size.height/2-25, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(red: 0.2275, green: 0.2, blue: 0.3294, alpha: 1.0)
        self.view.addSubview(loader)
        
        buyButton.setImage(UIImage(named: NSLocalizedString("buy_button", comment: "BuyButtonGreen_other")), for: .normal)
        
    }
    
    func loadTicket(_ route_id :String) -> Void {
        self.loader.startAnimating()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let user = self.getUser()
        let urlString = appDelegate.API_URL+"route/route_id/"+route_id+"/user_token/"+(user?.token!)!
        Alamofire.request(urlString, parameters: nil)
            .validate()
            .responseJSON {response in
                if(response.result.isSuccess) {
                    let json = response.result.value! as! [String:AnyObject]
                    self.ticket = json as NSDictionary!
                    let stationFrom = self.getStationByID(id: json["stationFrom"] as! String)
                    let stationTo = self.getStationByID(id: json["stationTo"] as! String)
                    self.fromLabel.text = NSLocalizedString("from", comment: "From: ")+(stationFrom?.title)!
                    self.toLabel.text = NSLocalizedString("to", comment: "To: ")+(stationTo?.title!)!
                    self.timeLabel.text = (json["departure"] as! String)
                    self.freeSeatsLabel.text = (json["seats"] as! String)
                    self.buyButton.isEnabled = true
                    self.freeSeatsLabel.isHidden = false
                    self.freeSeatsTitle.isHidden = false
                    let freeSeats = json["seats"] as? String
                    let freeSeatsInt = Int(freeSeats!)!
                    if(freeSeatsInt == 1) {
                        self.freeSeatsLabel.text = freeSeats!+NSLocalizedString("1_seat", comment: " seat")
                        self.ticketInfoLabel.text = NSLocalizedString("ticket_available", comment: "– TICKET IS AVAILABLE –")
                    } else if(freeSeatsInt >= 2 && freeSeatsInt <= 4) {
                        self.freeSeatsLabel.text = freeSeats!+NSLocalizedString("2_seat", comment: " seats")
                        self.ticketInfoLabel.text = NSLocalizedString("ticket_available", comment: "– TICKET IS AVAILABLE –")
                    } else if(freeSeatsInt == 0) {
                        self.buyButton.isEnabled = false
                        self.freeSeatsLabel.text = freeSeats!+NSLocalizedString("x_seats", comment: " seats")
                        self.freeSeatsLabel.isHidden = true
                        self.freeSeatsTitle.isHidden = true
                        self.ticketInfoLabel.text = NSLocalizedString("ticket_notavailable", comment: "– TICKET IS AVAILABLE –")
                    } else {
                        self.freeSeatsLabel.text = freeSeats!+NSLocalizedString("x_seats", comment: " seats")
                        self.ticketInfoLabel.text = NSLocalizedString("ticket_available", comment: "– TICKET IS AVAILABLE –")
                    }
                    
                    
                    var dateArray = (json["date"] as! String).components(separatedBy: "-")
                    self.dateLabel.text = String(dateArray[2].characters.prefix(2))+"/"+dateArray[1]+"/"+dateArray[0]
                    
                } else {
                    self.mainVC.dismiss(animated: true, completion: nil)
                }
                self.loader.stopAnimating()
        }
    }
    
    @IBAction func closeModal(_ sender: AnyObject) {
        mainVC.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buyTicket(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: ticket["date"] as! String)
        
        dateFormatter.dateFormat = "yyyyMMdd"
        let dateFormatted = dateFormatter.string(from: date!)
        let stationFrom = ticket["stationFrom"] as! String
        let stationTo = ticket["stationTo"] as! String
        let tarif = ticket["tarif"] as! String
        
        let urlString = "http://jizdenky.studentagency.sk/m/Booking/from/"+stationFrom+"/to/"+stationTo+"/tarif/"+tarif+"/departure/"+dateFormatted+"/retdep/"+dateFormatted+"/return/False/credit/True"
        
        UIApplication.shared.openURL(URL(string: urlString)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
