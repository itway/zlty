//
//  DottedView.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 01.08.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit

class DottedView: UIView {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(2.0)
        context?.setStrokeColor(UIColor.black.cgColor)
        let dashArray:[CGFloat] = [2,4]
        context?.setLineDash(phase: 1, lengths: dashArray)
        context?.move(to: CGPoint(x: 0, y: 0))
        context?.addLine(to: CGPoint(x: rect.width, y: 0))
        context?.strokePath()
    }

}
