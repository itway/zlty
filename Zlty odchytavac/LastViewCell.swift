//
//  FavouriteViewCell.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 27.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit

class LastViewCell: UITableViewCell {

    @IBOutlet weak var fromStationLabel: UILabel!
    @IBOutlet weak var toStationLabel: UILabel!
}
