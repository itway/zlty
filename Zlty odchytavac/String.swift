//
//  String.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 20.10.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit

extension String {
    var JSONString: AnyObject? {
        let data = self.data(using: .utf8)
        
        if let jsonData = data {
            do {
                let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                if let jsonResult = message as? NSMutableArray {
                    return jsonResult //Will return the json array output
                }
                else {
                    return nil
                }
            }
            catch let error as NSError {
                print("An error occurred: \(error)")
                return nil
            }
        }
        else {
            return nil
        }
    }
}
