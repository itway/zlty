//
//  UIViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 20.10.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {
    
    func getUser() -> User?  {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<User>
        if #available(iOS 10.0, *) {
            request = User.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "User")
        }
        do {
            let users = try context.fetch(request)
            return users.first!
        } catch let error {
            print("Error in getUser: "+error.localizedDescription)
        }
        return nil
    }
    
    func getStationByID(id: String) -> Station? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        let request: NSFetchRequest<Station>
        if #available(iOS 10.0, *) {
            request = Station.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: "Station")
        }
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let stations = try context.fetch(request)
            return stations.first!
        } catch let error {
            print("Error in getStationByID: "+error.localizedDescription)
        }
        return nil
    }
    
    func previousViewController() -> UIViewController? {
        if let stack = self.navigationController?.viewControllers {
            for i in (1..<stack.count).reversed() {
                if(stack[i] == self) {
                    return stack[i-1]
                }
            }
        }
        return nil
    }
    
}
