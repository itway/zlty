//
//  NotificationView.swift
//  
//
//  Created by Patrik Dendis on 22.07.16.
//
//

import UIKit

@IBDesignable class NotificationView: UIView {

    var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet var backView: UIView!
    
    @IBInspectable var titleLabelText : String? {
        get{
            return titleLabel.text;
        }
        set(titleLabelText) {
            titleLabel.text = titleLabelText;
        }
    }
    
    @IBInspectable var textLabelText : String? {
        get{
            return textLabel.text;
        }
        set(textLabelText) {
            textLabel.text = textLabelText!;
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NotificationView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
    }
}
