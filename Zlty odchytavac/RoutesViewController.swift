//
//  RoutesViewController.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 22.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftOverlays

class RoutesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var stationFromLabel: UILabel!
    @IBOutlet weak var stationToLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var notFoundImage: UIImageView!
    
    var notificationView: NotificationView!
    var routes: [AnyObject] = []
    var loader:  NVActivityIndicatorView!
    var stationFrom: Station!
    var stationTo: Station!
    var date: Date!
    var tarif: String!
    var tarif_text: String!
    var currency: String!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loader = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2-25, y: self.view.frame.size.height/2+5, width: 50, height: 50))
        loader.type = .ballClipRotatePulse
        loader.color = UIColor(red: 0.2275, green: 0.2, blue: 0.3294, alpha: 1.0)
        self.view.addSubview(loader)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("download_stations", comment: "Loading..."))
        tableView.addSubview(refreshControl!)
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        stationFromLabel.text = (NSLocalizedString("from", comment: "From: ")+(stationFrom.title)!).uppercased()
        stationToLabel.text = (NSLocalizedString("to", comment: "To: ")+(stationTo.title)!).uppercased()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateLabel.text = NSLocalizedString("day", comment: "DAY: ")+dateFormatter.string(from: date)
        
    }
        
    override func viewDidAppear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        notificationView = NotificationView(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: appDelegate.NAV_HEIGHT+5))
        
        let user = self.getUser()
        let newLastRoute = NSEntityDescription.insertNewObject(forEntityName: "LastRoute", into: managedContext) as! LastRoute
        newLastRoute.stationFrom = stationFrom.title
        newLastRoute.stationTo = stationTo.title
        newLastRoute.date = NSDate()
        user?.last_tarif = tarif_text
        do {
            try managedContext.save()
            try user?.managedObjectContext?.save()
        } catch {
            fatalError("Failure to save context new Last route and update user: \(error)")
        }
        
        loader.startAnimating()
        self.loadRoutes()
    }
    
    func loadRoutes() -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let dateFormatted = dateFormatter.string(from: date)
        let user = self.getUser()
        var urlString = appDelegate.API_URL+"routes/from/"+stationFrom.id!+"/to/"+stationTo.id!
        urlString += "/tarif/"+tarif+"/date/"+dateFormatted+"/currency/"+currency
        urlString += "/user_token/"+(user?.token)!
        Alamofire.request(urlString, parameters: nil)
            .validate()
            .responseString {response in
                if(response.result.isSuccess) {
                    let json = response.result.value!.JSONString
                    self.routes = (json as! NSArray) as Array
                    if(self.routes.count > 0) {
                        self.tableView.reloadData()
                        self.errorView.isHidden = true
                    } else {
                        self.errorView.isHidden = false
                        self.notFoundImage.image = UIImage(named: NSLocalizedString("notFound_image", comment: "NotFound_other"))
                    }

                } else {
                    if(response.response?.statusCode != 300) {
                        self.notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops... A problem has occured")
                        self.notificationView.textLabelText = NSLocalizedString("error_connection_text", comment: "We are working to correct the issue.")
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 1.5)
                    } else {
                        self.errorView.isHidden = false
                    }
                }
                self.loader.stopAnimating()
                self.refreshControl.endRefreshing()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    @IBAction func closeModal(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    
    //MARK - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RoutesViewCell
        let index = (indexPath as NSIndexPath).row as Int
        let obj = routes[index] as! NSDictionary
        if(self.view.frame.size.width < 375) {
            cell.fromStationLabel.isHidden = true
            cell.toStationLabel.isHidden = true
        }
        
        cell.selectionStyle = .none
        cell.arrivalLabel.text = obj["arrival"] as? String
        cell.departureLabel.text = obj["departure"] as? String
        cell.seatsLabel.text = obj["seats"] as? String
        cell.fromStationLabel.text = NSLocalizedString("from", comment: "From: ")+self.stationFrom.title!
        cell.toStationLabel.text = NSLocalizedString("to", comment: "To: ")+self.stationTo.title!
        if(self.currency == "EUR") {
            cell.priceLabel.text = (obj["price"] as? String)!+"€"
        } else {
            cell.priceLabel.text = (obj["price"] as? String)!+"Kč"
        }
        
        let typeString = obj["type"] as? String
        if(typeString == "Coach" || typeString == "Autobus") {
            cell.typeIcon.image = UIImage(named: "IconCoach")
        } else if(typeString == "Rail car" || typeString == "Vlak") {
            cell.typeIcon.image = UIImage(named: "IconTrain")
        } else {
            cell.typeIcon.image = UIImage(named: "IconBusTrain")
        }
        
        if((obj["seats"] as? String) == "1") {
            cell.freeTextLabel.text = NSLocalizedString("1_ticket", comment: " available ticket")
        } else if((obj["seats"] as? String) == "2" || (obj["seats"] as? String) == "3" || (obj["seats"] as? String) == "4") {
            cell.freeTextLabel.text = NSLocalizedString("2_ticket", comment: " available tickets")
        } else {
            cell.freeTextLabel.text = NSLocalizedString("x_tickets", comment: " available tickets")
        }
        
        cell.typeIcon.image = cell.typeIcon.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        if((obj["seats"] as? String) == "0") {
            let redColor = UIColor(red: 0.8157, green: 0.0078, blue: 0.1059, alpha: 1.0)
            cell.typeIcon.tintColor = redColor
            cell.arrivalLabel.textColor = redColor
            cell.departureLabel.textColor = redColor
            cell.delimiterView.backgroundColor = redColor
            cell.leftView.backgroundColor = UIColor(red: 0.9529, green: 0.8902, blue: 0.898, alpha: 1.0)
            cell.priceTextLabel.isHidden = true
            cell.priceLabel.text = ""
            cell.seatsLabel.text = ""
            cell.freeTextLabel.text = NSLocalizedString("sold_out", comment: "Sold out")
            cell.buyButton.isEnabled = false
            
        } else {
            let grayColor = UIColor(red: 0.4627, green: 0.4627, blue: 0.4627, alpha: 1.0)
            cell.typeIcon.tintColor = grayColor
            cell.arrivalLabel.textColor = grayColor
            cell.departureLabel.textColor = grayColor
            cell.delimiterView.backgroundColor = grayColor
            cell.leftView.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)
            cell.priceTextLabel.isHidden = false
            cell.buyButton.isEnabled = true
        }
        
        if(obj["saved"] as? Bool)! {
            cell.saveButton.isSelected = true
        } else {
            cell.saveButton.isSelected = false
        }
        
        self.view.needsUpdateConstraints()
        
        cell.saveButton.tag = index
        
        return cell
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        let obj = routes[sender.tag] as! NSDictionary
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateFormated = dateFormatter.string(from: self.date)+"-"+(obj["departure"] as? String)!
        let user = self.getUser()
        let language = NSLocale.current.languageCode!
        
        let parameters: Parameters = [
            "fromStation": stationFrom.id!,
            "toStation": stationTo.id!,
            "userToken": user!.token!,
            "dateTime": dateFormated,
            "tarif": self.tarif,
            "currency": self.currency,
            "freeSeats": (obj["seats"] as? String)!,
            "lang": language
        ]
        
        Alamofire.request(String(appDelegate.API_URL+"newTrackingRoute"), method: .post, parameters: parameters)
            .validate()
            .responseString {response in
                if(response.result.isSuccess){
                    if(response.response?.statusCode == 200) {
                        sender.isSelected = true
                        self.notificationView.titleLabelText = NSLocalizedString("connection_saved", comment: "Great! Connection is saved")
                        self.notificationView.textLabelText = NSLocalizedString("connection_saved_text", comment: "We will notify you if something changes.")
                        self.notificationView.backView.backgroundColor = UIColor(red:0.05, green:0.83, blue:0.34, alpha:1.0)
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 1.5)
                    } else {
                        sender.isSelected = false
                    }
                } else {
                    if(response.response?.statusCode != 300) {
                        sender.isSelected = false
                        self.notificationView.titleLabelText = NSLocalizedString("error_occured", comment: "Oops... A problem has occured")
                        self.notificationView.textLabelText = NSLocalizedString("error_connection_text", comment: "We are working to correct the issue.")
                        self.notificationView.backView.backgroundColor = UIColor(red:0.76, green:0.00, blue:0.09, alpha:1.0)
                        UIViewController.showNotificationOnTopOfStatusBar(self.notificationView, duration: 1.5)
                    }
                }
        }
    }
    
    @IBAction func buyAction(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let dateFormatted = dateFormatter.string(from: date)
        
        var urlString = "http://jizdenky.studentagency.sk/m/Booking/from/"+stationFrom.id!+"/to/"+stationTo.id!
        urlString += "/tarif/"+tarif+"/departure/"+dateFormatted
        urlString += "/retdep/"+dateFormatted+"/return/False/credit/True"
        print(urlString)
        UIApplication.shared.openURL(URL(string: urlString)!)

    }
    
    func refreshTable() {
        self.loadRoutes()
    }

}
