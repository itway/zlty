//
//  RoutesViewCell.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 22.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import UIKit

class RoutesViewCell: UITableViewCell {

    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var fromStationLabel: UILabel!
    @IBOutlet weak var toStationLabel: UILabel!
    @IBOutlet weak var departureLabel: UILabel!
    @IBOutlet weak var arrivalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var typeIcon: UIImageView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var freeTextLabel: UILabel!
    @IBOutlet weak var priceTextLabel: UILabel!
    @IBOutlet weak var delimiterView: UIView!
    @IBOutlet weak var topSpaceFreeTextLabel: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
}
