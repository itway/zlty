//
//  FormTextField.swift
//  Zlty odchytavac
//
//  Created by Patrik Dendis on 18.07.16.
//  Copyright © 2016 ITway.sk. All rights reserved.
//

import Foundation
import UIKit

class FormTextField: UITextField {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(red:0.62, green:0.58, blue:0.73, alpha:1.0).cgColor

    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 8, width: bounds.size.width - 27, height: bounds.size.height - 16);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    func showFocus() {
        self.layer.borderColor = UIColor(red:0.05, green:0.83, blue:0.34, alpha:1.0).cgColor
        self.perform(#selector(self.disableFocus), with: nil, afterDelay: 0.3)
    }
    
    func disableFocus() {
        self.layer.borderColor = UIColor(red:0.62, green:0.58, blue:0.73, alpha:1.0).cgColor
    }
    
}
